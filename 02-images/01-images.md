# Images

```bash

# History
docker image history <image:tag>

# Inspect a image
docker image inspect <image:tag>

# Add tag
docker image tag <image> <image/tag>
docker image tag nginx mauromamani/nginx
docker image tag nginx mauromamani/nginx:latest

# Push image
docker image push <image>

## Dockerfile sample 2
WORKDIR:
  - Una manera de cambiar de directorio, como usar un cd pero con docker
COPY:
  - Copiar archivos:
    - <archivoLocal> <archivoContainer>
```
