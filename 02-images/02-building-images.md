# Building Images

```bash
# Dockerfile
Es un recipiente que contine tu imagen

* Docker se encarga de los loggs, no es necesario que nosotros lo manejemos

FROM:
  - Tiene que estar siempre
  - Generalmente es una distribucion minima

ENV:
  - Podemos setear env variables
  - EX: ENV KEY VALUE

RUN:
  - Ejecutar commandos, shell scripts

EXPOSE:
  - Por default ningun puerto TCP o UDP esta abierto dentro de un container
  - Los puertos que pongamos acá no van a estar abiertos de manera automatica, eso lo tenemos que hacer con el comando -p

CMD:
  - Comando requerido, es el ultimo comando que se va a ejecutar


# Build image
docker image build -t customNginx .
-t: Tag de la imagen
.: Le decimos que queremos construirla en el mismo directorio

## dockerfile-assignment 1
docker container run -p 80:3000 --rm mauromamani/testing-node
```
