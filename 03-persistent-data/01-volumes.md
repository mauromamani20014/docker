# Volumes

```bash
# Una de las manera de decirle a un contenedor que debe tener en cuenta un "volum" es en el Dockerfile
# Los volume necesitan eliminarse de manera manual, no se eliminan de manera automatica removiendo el container

# Eliminar volumenes que no se utilizan
docker volume prune

# Listar volumenes
docker volume ls

# Eliminar volumenes
docker volume rm <id>

# Cuando creamos un container podemos usar -v para especificar el nombre del volume del container
# o para decir que tal container va a usar tal volumen
  docker container run -d --name mysql -e MYSQL_ALLOW_EMPTY_PASSWORD=True -v mysql-db:/var/lib/mysql mysql
# el nombre se asigna con los <nombre-volume>:/<ubicacion>

## Bind mounting: Podemos usar un directorio como volumen para nuestro container, los cambios que hagamos
## en el directorio se veran reflejados en el container

docker container run -d  -v $(pwd):/site <container>


```
