# Commands

```sh
# Muestra la informacion de los pods activos
kubectl get pods

# Ejecuta el comando en un pod activo
kubectl exec -it <pod-name> <command>

# Muestra los logs de un pod
kubectl logs <pod-name>

# Eliminar un pod
kubectl delete pod <pod-name>

# Decirle a kubernetes de procesar la siguiente configuracion
kubectl apply -f <config-file-name>

# Muestra algo de informacion del pod activo
kubectl describe pod <pod-name>
  ```
