# Pod Spec

```sh

#
apiVersion: 1

# El tipo de objeto que queremos crear
kind: Pod

# Configuraciones para el objeto que estamos por crear
metadata:
  # Cuando el Pod sea creado le damos el nombre de "posts"
  name: posts

# Los atributos que queremos que contenga el objeto que estamos por crear
spec:
  # Podemos crear muchos containers en un solo Pod
  containers:
      # La imagen que queremos usar
    - image:
      # El nombre del container
      name:
```
