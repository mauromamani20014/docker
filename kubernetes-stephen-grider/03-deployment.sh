# Deployment

```sh
# Un Deployment es un objeto que maneja un grupod de Pods, estos Pods contienen la misma configuracion
# nos ayuda a mantener siempre la misma cantidad de Pods, y de facilitar la modificacion en cada Pod

# Obtener todos los deployments activos
kubectl get deployments

# Muestra informacion sobre el deployment
kubectl describe deployment <depl-name>

# Crear un deployment
kubectl apply -f <config-file>

# Borrar un deployment
kubectl delete deployment <depl-name>

# Reiniciar un deployment(falta explicacion)
kubectl rollout restart deployment <depl-name>
```
