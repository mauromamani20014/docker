# Containers

```bash
# Run a container
docker container run -d -p 8080:80 --name=mysite nginx

-d : run in background
-p : expose
--rm : eliminar el container cuando es creado
--name : give it a name

# List running containers
docker container ls

# List all containers
docker container ls -a

# Stop container
docker container stop <id-contaier>

# Start container
docker container start <id-container>

# Remove container: container must be stopped
docker container rm <id-container>

# See container logs
docker container logs <id-container>

# See process of container
docker container top <id-container>

# See info about container
docker container inspect <id-container>

# Watch realtime stats of container
docker container stats







```
