# Docker Networks

```bash
# Show networks
docker network ls

# Inspect network
docker network inspect

# Create network
docker network create <name> --driver
: default driver: bridge

# Run container with a network
docker container run -d --name new_nginx --network <network_name> ngix

# Attach docker container to a network
docker network connect <id-network> <id-container>

# Disconnect container network
docker network disconnect <id-network> <id-container>
```
