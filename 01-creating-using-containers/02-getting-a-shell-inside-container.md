# Getting a shell inside containers

```bash
# Get a shell
docker container run -it <image> <command>

## Example
docker container run -it ngix bash

# Start a container with shell
docker container start -ai <id-container>

# Run additional command in started container 
docker container exec -it <id-container> <command>

## Example
docker container exec -it mysql bash
```
