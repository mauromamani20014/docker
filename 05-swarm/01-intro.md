# Introduction Docker Swarm

```sh
# Iniciar swarm
docker swarm init

# Listar los nodos
docker node ls

# Listar los servicios
docker service ls

# Ver un servicio
docker service ps <nombre-service>

# Escalar un servicio
docker service update <id> --replicas 3
--replicas:

# Detener el/los servicios
docker service rm <nombre-service>

## Services en nodes| distintos servidores
cuando usamos docker swarm init nos sale una advertencia, es necesario usar un IP que sea accesible para
los otros servidores, tenemos que usar la ip publica (la primera opcion)

docker swarm init --advertise-addr <ip>

Nos devuelve un comando para unir a los demas servidores, lo tenemos que pegar alli

""
docker swarm join --token SWMTKN-1-2bduvbxnflxtj3hkqbuzaspf538v143ma7w16znj8zedchu37v-35zbgkqk7uf62qt9rc9plkthb 172.18.0.6:2377
""


```
